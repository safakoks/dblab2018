create index abc on Customers(CustomerName);
explain select * from Customers order by CustomerName;
alter table Customers drop index abc;
create view speacialView as select * from Customers order by CustomerName;
select * from speacialView;
insert into speacialView values (95, 'Ana holmes', 'Ana Holmes', 'Kotekli', 'Mugla', '48000', 'Turkey');
select * from speacialView