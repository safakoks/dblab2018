# 1. Show the films whose budget is greater than 10 million$ and ranking is less than 6.
select * from movies where budget > 10000000;
# 2. Show the action films whose rating is greater than 8.8 and produced after 2009.
select m.* from movies m 
join genres g 
on g.movie_id = m.movie_id 
where g.genre_name = 'Action' and m.ranking > 8.8 and m.year > 2009;
# 3. Show the drama films whose duration is more than 150 minutes and oscars is more than 2.
select m.* from movies m 
join genres g 
on g.movie_id = m.movie_id 
where g.genre_name = 'Drama' and m.oscars > 2 and m.duration > 150;
# 4. Show the films that Orlando Bloom and Ian McKellen have act together and has more than 2 Oscars.
select distinct m.* from movies m 
join movie_stars ms 
on m.movie_id = ms.movie_id 
where ms.star_id
in (select s.star_id from stars s
where s.star_name = 'Orlando Bloom' 
or s.star_name = 'Ian McKellen') and m.oscars > 2;
# 5. Show the Quentin Tarantino films which have more than 500000 votes and produced before 2000.	 
select m.* from movies m 
join movie_directors md 
on m.movie_id = md.movie_id
join directors d 
on d.director_id = md.director_id
where d.director_name = 'Quentin Tarantino' and m.votes > 500000 and m.year < 2000;
# 6. Show the thriller films whose budget is greater than 25 million$.	 
select m.* from movies m 
join genres g 
on g.movie_id = m.movie_id 
where g.genre_name = 'Thriller' and m.budget > 25000000;
# 7. Show the drama films whose language is Italian and produced between 1990-2000.	
select m.* from movies m 
join genres g 
on g.movie_id = m.movie_id 
join languages l 
on m.movie_id = l.movie_id
where g.genre_name = 'Drama' and l.language_name = 'Italian' and m.year between 1990 and 2000;
# 8. Show the films that Tom Hanks has act and have won more than 3 Oscars.	 
select distinct m.* from movies m 
join movie_stars ms 
on m.movie_id = ms.movie_id 
where ms.star_id
in (select s.star_id from stars s
where s.star_name = 'Tom Hanks') and m.oscars > 3;
# 9. Show the history films produced in USA and whose duration is between 100-200 minutes.
select m.* from movies m 
join genres g 
on g.movie_id = m.movie_id 
where g.genre_name = 'History'
and m.duration between 100 and 200;
# 10.Compute the average budget of the films directed by Peter Jackson.
select avg(m.budget) as 'Average of Budget'from movies m 
join movie_directors md 
on m.movie_id = md.movie_id
join directors d 
on d.director_id = md.director_id
where d.director_name = 'Peter Jackson';
# 11.Show the Francis Ford Coppola film that has the minimum budget.
select min(m.budget) as 'Minimum Budget'from movies m 
join movie_directors md 
on m.movie_id = md.movie_id
join directors d 
on d.director_id = md.director_id
where d.director_name = 'Francis Ford Coppola';
# 12.Show the film that has the most vote and has been produced in USA.

