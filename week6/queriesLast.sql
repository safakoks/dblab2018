# 1. Show the films whose budget is greater than 10 million$ and ranking is less than 6.

select * from movies where budget > 10000000;

# 2. Show the action films whose rating is greater than 8.8 and produced after 2009.

select m.* from movies m 
join genres g 
on g.movie_id = m.movie_id 
where g.genre_name = 'Action' and m.ranking > 8.8 and m.year > 2009;

# 3. Show the drama films whose duration is more than 150 minutes and oscars is more than 2.

select m.* from movies m 
join genres g 
on g.movie_id = m.movie_id 
where g.genre_name = 'Drama' and m.oscars > 2 and m.duration > 150;

# 4. Show the films that Orlando Bloom and Ian McKellen have act together and has more than 2 Oscars.

select distinct m.* from movies m 
join movie_stars ms 
on m.movie_id = ms.movie_id 
where ms.star_id
in (select s.star_id from stars s
where s.star_name = 'Orlando Bloom' 
or s.star_name = 'Ian McKellen') and m.oscars > 2;

# 5. Show the Quentin Tarantino films which have more than 500000 votes and produced before 2000.
	 
select m.* from movies m 
join movie_directors md 
on m.movie_id = md.movie_id
join directors d 
on d.director_id = md.director_id
where d.director_name = 'Quentin Tarantino' and m.votes > 500000 and m.year < 2000;

# 6. Show the thriller films whose budget is greater than 25 million$.	 

select m.* from movies m 
join genres g 
on g.movie_id = m.movie_id 
where genre_name = 'Thriller' and budget > 25000000;

# 7. Show the drama films whose language is Italian and produced between 1990-2000.	
-- select m.* from movies m 
-- join genres g 
-- on g.movie_id = m.movie_id 
-- join languages l 
-- on m.movie_id = l.movie_id
-- where g.genre_name = 'Drama' and l.language_name = 'Italian' and m.year between 1990 and 2000;

select title 
from movies
where year between 1990 and 2000 and movie_id in (
	select genres.movie_id 
    from genres 
    join languages 
    on genres.movie_id=languages.movie_id 
    where genre_name = "Drama" and language_name = "Italian"
);

# 8. Show the films that Tom Hanks has act and have won more than 3 Oscars.	 

select distinct m.* from movies m 
join movie_stars ms 
on m.movie_id = ms.movie_id 
where ms.star_id
in (select s.star_id from stars s
where s.star_name = 'Tom Hanks') and m.oscars > 3;

# 9. Show the history films produced in USA and whose duration is between 100-200 minutes.

select title 
from movies join genres on movies.movie_id = genres.movie_id
where genres.genre_name = "History" and duration between 100 and 200 and movies.movie_id in (
	select movie_id 
    from countries join producer_countries on countries.country_id = producer_countries.country_id
    where country_name = "USA"
);

# 10.Compute the average budget of the films directed by Peter Jackson.

select avg(m.budget) as 'Average Budget'from movies m 
join movie_directors md 
on m.movie_id = md.movie_id
join directors d 
on d.director_id = md.director_id
where d.director_name = 'Peter Jackson';

# 11.Show the Francis Ford Coppola film that has the minimum budget.

select min(m.budget) as 'Minimum Budget'from movies m 
join movie_directors md 
on m.movie_id = md.movie_id
join directors d 
on d.director_id = md.director_id
where d.director_name = 'Francis Ford Coppola';

# 12.Show the film that has the most vote and has been produced in USA.

select title, votes
from movies where movie_id in (
	select movie_id from producer_countries pc join countries c on pc.country_id=c.country_id where c.country_name = "USA"
) 
order by votes desc limit 1;



