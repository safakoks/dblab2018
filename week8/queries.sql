use company;

update Customers
set ContactName = "Şafak Öksüzer"
where CustomerID = 3;

update Customers
set ContactName = "Şafak Öksüzer", City = "Adana"
where Country = "Argentina";

select OrderID
from Orders
where CustomerID = 3;

delete from orderdetails
where OrderID = 10365;

delete from Orders
where CustomerID=3;

delete from customers
where CustomerID=3;

select city from Customers
union
select city from Suppliers;

