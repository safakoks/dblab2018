select count(customers.CustomerID) as numberOfCustomers, customers.Country
from customers
group by customers.Country
order by numberOfCustomers desc;

select count(products.productID) as numberOfProducts, suppliers.SupplierName
from products join suppliers on products.SupplierID = suppliers.SupplierID
group by products.SupplierID
order by numberOfProducts desc;

select * from customers;
show variables like '--secure_file_priv';

load data infile "employees.csv"
into table employees
fields terminated by ','
ignore 1 lines;

load data infile "customers.csv"
into table customers
fields terminated by ';'
ignore 1 lines;

load data infile "categories.tsv"
into table categories;